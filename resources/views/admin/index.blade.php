<!DOCTYPE html>
<html>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
<head>
	<title>Admin - Toko Buku</title>
</head>
<body>

<div class="container mt-3 mb-4">

	<a class="btn btn-primary btn-md" href="/admin/create_book">
		Tambah Buku Baru
	</a>

	&nbsp;&nbsp;&nbsp;

	<a class="btn btn-secondary btn-md" href="/admin/user">
		Manajemen User
	</a>

	&nbsp;&nbsp;&nbsp;

	<a class="btn btn-danger btn-md" href="/Auth/logout">
		Keluar
	</a>

	<br><br>

	<table class="table thead-dark">
		<tr>
			<th>Gambar</th>
			<th>Nama</th>
			<th>Penulis</th>
			<th>Tautan</th>
			<th>Aksi</th>
		</tr>
		@foreach($data as $book)
		<tr>
			<tbody>
			<td><img src="{{$book->gambar}}" height="250" width="200"></td>
			<td>{{$book->nama}}</td>
			<td>{{$book->penulis}}</td>
			<td>{{$book->tautan}}</td>
			<td>
				<a href="/admin/edit/{{$book->id}}">Ubah</a>
				&nbsp;&nbsp;&nbsp;
				<a href="/admin/delete/{{$book->id}}">Hapus</a>
			</td>
			</tbody>
		</tr>
		@endforeach
	</table>

</div>

</body>
</html>