<!DOCTYPE html>
<html>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
<head>
	<title>Admin - Add Book</title>
</head>
<body>

<div class="container mt-3">
	<a class="btn btn-danger" href="/admin">
		Back
	</a>
	<div class="card mt-3 mb-4">
		<div class="card-header">
			<h4 class="title">ADD BOOKS</h4>
		</div>
		<div class="card-body">
 	<form action="/admin/add_book" method="POST">
		@csrf
		<div class="form-group">
		<label>
			Gambar :
		</label>
		<br>
		<input class="form-control" type="text" name="gambar">
		</div>
		<br>
		<div class="form-group">
		<label>
			Nama Buku :
		</label>
		<br>
		<input class="form-control" type="text" name="nama">
		</div>
		<br>
		<div class="form-group">
		<label>
			Nama Penulis :
		</label>
		<br>
		<input class="form-control" type="text" name="penulis">
		</div>
		<br>
		<div class="form-group">
		<label>
			Persediaan :
		</label>
		<br>
		<input class="form-control" type="number" name="persediaan">
		</div>
		<br>
		<div class="form-group">
		<label>
			Tautan :
		</label>
		<br>
		<input class="form-control" type="text" name="tautan">
		</div>
		<br>
		<div class="form-group">
		<label>
			Deskripsi :
		</label>
		<br>
		<textarea class="form-control" name="deskripsi"></textarea>
		</div>
		<br>
		<input class="btn btn-success" type="submit" name="add" value="add">
	</form>
		</div>
	</div>
</div>

<script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>
<script>
     CKEDITOR.replace('deskripsi');
</script>
</body>
</html>