<!DOCTYPE html>
<html>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
<head>
	<title>Detail - Toko Buku</title>
</head>
<body>

<div class="container mt-3 mb-3">
	@foreach($data as $dt)
	<div class="card mt-3">
		<div class="card-header">
			<h3 class="title">
				{{$dt->nama}} - Stok {{$dt->persediaan}}
			</h3>
		</div>
		<br>
		<div class="card-body">
		<img class="img-fluid mx-auto d-block" height="auto" src="{{$dt->gambar}}">
		<br>
			<p class="text-justify">
				{{$dt->deskripsi}}
			</p>
		</div>
		<div class="card-footer">
			<a class="btn btn-dark" href="{{$dt->tautan}}">
				Buy Now
			</a>
		</div>
	</div>
	@endforeach
</div>

</body>
</html>